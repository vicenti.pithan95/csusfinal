using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using Microsoft.Extensions.DependencyInjection;
using Telegram.Bot;
using Telegram.Bot.Args;
using System.Text.RegularExpressions;

namespace ApiSusAV
{
    public class Program
    {
        static ITelegramBotClient botClient;
        [Obsolete]
        public static void Main(string[] args)
        {
            Service.Bot bot = new Service.Bot();
            bot.StartBot();
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                })
                .ConfigureServices ( services => 
                {
                    services.AddHostedService<TimedHostedService> ();
                } );
    }
}

