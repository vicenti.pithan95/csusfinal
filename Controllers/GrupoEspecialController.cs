﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSusAV.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GrupoEspecialController : ControllerBase
    {

        [HttpGet]
        public IEnumerable<Model.GrupoEspecial> GetGruposEspeciais()
        {
            Model.DAL dal = new Model.DAL();

            List<Model.GrupoEspecial> listaGruposEspeciais = dal.GetGruposEspeciais();
            return listaGruposEspeciais;
        }

        [HttpGet("{id}")]
        public Model.GrupoEspecial GetGrupoEspecial(int id)
        {
            Model.GrupoEspecial c1 = new Model.GrupoEspecial();
            c1 = GetGruposEspeciais().Where(x => x.Id == id).FirstOrDefault();
            return c1;
        }
        [HttpGet("{nome}")]
        public Model.GrupoEspecial GetGrupoEspecialNmGrupo(String nome)
        {
            Model.GrupoEspecial c1 = new Model.GrupoEspecial();
            c1 = GetGruposEspeciais().Where(x => x.NmGrupo == nome).FirstOrDefault();
            return c1;
        }

        [HttpGet]
        [Route("comorbidade/{isComorbidade}")]
        public IEnumerable<Model.GrupoEspecial> GetGrupoEspecialComorbidade(Boolean isComorbidade)
        {
            List<Model.GrupoEspecial> listaGruposEspeciais = new List<Model.GrupoEspecial>();
            listaGruposEspeciais = GetGruposEspeciais().Where(x => x.IsComorbidade == isComorbidade).ToList();
            return listaGruposEspeciais;
        }

        [HttpPost]
        [Authorize]
        public ActionResult<string> CreateGrupoEspecial([FromBody]Model.GrupoEspecial grupoEspecial)
        {
            Model.DAL dal = new Model.DAL();
            Model.GrupoEspecial c1 = dal.CreateGrupoEspecial(grupoEspecial);
            if (!string.IsNullOrWhiteSpace(c1.NmGrupo)) {
                return Ok(c1);
            }
            return BadRequest("Grupo especial Já cadastrado");
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<string> EditComorbidade([FromBody] Model.GrupoEspecial grupoEspecial,int id)
        {
            Model.DAL dal = new Model.DAL();
            Model.GrupoEspecial c1 = new Model.GrupoEspecial
            {
                NmGrupo = grupoEspecial.NmGrupo,
                IsComorbidade = grupoEspecial.IsComorbidade,
                Id = id
            };
            Model.GrupoEspecial grupoEspecialDal = dal.EditGrupoEspecial(c1);
            if (!string.IsNullOrWhiteSpace(grupoEspecialDal.NmGrupo))
            {
                return Ok(grupoEspecialDal);
            }
            return BadRequest("Grupo Especial não encontrado");
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult<string> DeleteGrupoEspecial(int id)
        {
            Model.DAL dal = new Model.DAL();
           Boolean success  = dal.DeleteGrupoEspecial(id);
            if (success)
            {
                return Ok($"O Grupo Especial {id} foi deletado com sucesso");
            }
            return BadRequest("Grupo Especial não encontrado");
        }
    }
}

