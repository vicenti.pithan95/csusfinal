﻿using ApiSusAV.Model;
using ApiSusAV.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSusAV.Controllers
{
    [Route( "api/account" )]
    [ApiController]
    public class HomeController : ControllerBase
    {
        [HttpPost]
        [Route("login")]
        public async Task<ActionResult<dynamic>> Authenticate([FromBody] User model)
        {
            // Recupera o usuário
            DAL dal = new DAL();
            User user = new User();
            model.Password = HashPassword.CalculaHash(model.Password);
            user = dal.GetUsers().Where(x => x.Username.ToLower() == model.Username.ToLower() && model.Password == x.Password).FirstOrDefault();

            // Verifica se o usuário existe
            if (user == null)
                return NotFound(new { message = "Usuário ou senha inválidos" });

            // Gera o Token
            var token = TokenService.GenerateToken(user);

            // Oculta a senha
            user.Password = "";

            // Retorna os dados
            return new
            {
                user = user,
                token = token
            };
        }

        [HttpPost]
        [Authorize]
        [Route("create")]
        public async Task<ActionResult<dynamic>> Create([FromBody] User model)
        {
            // Cria um usuário
            DAL dal = new DAL();
            User user = new User();
            model.Password = HashPassword.CalculaHash(model.Password);
            model.Role = "employee";
            user = dal.CreateUser(model);
            if (user.Id != 0)
            {
                return Ok(user);
            }
            return BadRequest("Usuário já cadastrado.");

        }
        
        [HttpGet("{nome}")]
        [Authorize]
        public User getUser(String nome)
        {
            
            DAL dal = new DAL();
            User user = dal.GetUser(nome);
            if (user.Id != 0)
            {
                return user;
            }
            return user;

        }
        [HttpGet]
        [Route( "anonymous" )]
        [AllowAnonymous]
        public string Anonymous() => "Anônimo";

        [HttpGet]
        [Route( "authenticated" )]
        [Authorize]
        public string Authenticated() => String.Format( "Autenticado - {0}", User.Identity.Name );

        [HttpGet]
        [Route( "employee" )]
        [Authorize( Roles = "employee,manager" )]
        public string Employee() => "Funcionário";

        [HttpGet]
        [Route( "manager" )]
        [Authorize( Roles = "manager" )]
        public string Manager() => "Gerente";
    }
}

