﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSusAV.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CidadaoController : ControllerBase
    {
        [HttpGet]
        [Route("vacinacao/{email}")]
        public ActionResult<Boolean> Vacinacao( String email)
        {
            Model.DAL dal = new Model.DAL();
            if (dal.Vacinacao( email ))
            {
                return true;
            }
            return false;
        }
        [HttpGet]
        [Route( "vacinacao/bot/{email}" )]
        public Boolean VacinacaoBot( String email )
        {
            Model.DAL dal = new Model.DAL();
            if (dal.Vacinacao( email ))
            {
                return true;
            }
            return false;
        }

        [HttpGet]
        [Route( "email/{email}" )]
        public ActionResult<Boolean> isEmailDuplicado( String email )
        {
            Model.DAL dal = new Model.DAL();
            Boolean success = dal.EmailDuplicado( email );
            if (success)
            {
                return true;
            }
            return  false;
        }
        [HttpGet]
        [Route( "email/bot/{email}" )]
        public Boolean CidadaoCadastrado( String email )
        {
            Model.DAL dal = new Model.DAL();
            Boolean success = dal.EmailDuplicado( email );
            if (success)
            {
                return true;
            }
            return false;
        }
        [HttpGet]
        [Authorize]
        public IEnumerable<Model.Cidadao> GetCidadaos()
        {
            Model.DAL dal = new Model.DAL();

            List<Model.Cidadao> listaCidadao = dal.GetCidadaos();
            List<Model.Cidadao> listCompleta = new List<Model.Cidadao>();
            Model.Local local = new Model.Local();
            Model.GrupoEspecial grupo = new Model.GrupoEspecial();
            foreach (Model.Cidadao cidadao in listaCidadao)
            {
                local = dal.GetLocais().Where( x => x.Id == cidadao.Local.Id ).FirstOrDefault();
                grupo = dal.GetGruposEspeciais().Where( x => x.Id == cidadao.GrupoEspecial.Id ).FirstOrDefault();

                if (grupo != null && local != null)
                {
                    cidadao.Local = local;
                    cidadao.GrupoEspecial = grupo;
                }
                listCompleta.Add( cidadao );
            }
            return listCompleta;
        }

        [HttpGet("{id}")]
        public Model.Cidadao GetCidadao(int id)
        {
            Model.DAL dal = new Model.DAL();
            Model.Cidadao cidadao = new Model.Cidadao();
            cidadao = dal.GetCidadaos().Where(x => x.Id == id).FirstOrDefault();

            Model.Local local = new Model.Local();
            local = dal.GetLocais().Where(x => x.Id == cidadao.Local.Id).FirstOrDefault();
            Model.GrupoEspecial grupo = dal.GetGruposEspeciais().Where( x => x.Id == cidadao.GrupoEspecial.Id ).FirstOrDefault();

            if ( grupo != null && local != null )
            {
                cidadao.Local = local;
                cidadao.GrupoEspecial = grupo;
            }
            else
            {
                return cidadao;
            }
            return cidadao;
        }

        [HttpGet( "bot/bot" )]
        public Model.Cidadao GetCidadaoBot( int id )
        {
            Model.DAL dal = new Model.DAL();
            Model.Cidadao cidadao = new Model.Cidadao();
            cidadao = dal.GetCidadaosBot().Where( x => x.Id == id ).FirstOrDefault();

            Model.Local local = new Model.Local();
            local = dal.GetLocais().Where( x => x.Id == cidadao.Local.Id ).FirstOrDefault();
            Model.GrupoEspecial grupo = dal.GetGruposEspeciais().Where( x => x.Id == cidadao.GrupoEspecial.Id ).FirstOrDefault();

            if (grupo != null && local != null)
            {
                cidadao.Local = local;
                cidadao.GrupoEspecial = grupo;
            }
            else
            {
                return cidadao;
            }
            return cidadao;
        }

        [HttpPost]
        public ActionResult<string> CreateCidado([FromBody] Model.Cidadao cidadao)
        {
            Model.DAL dal = new Model.DAL();
            Model.Local local = dal.GetLocais().Where(x => x.Id == cidadao.Local.Id).FirstOrDefault();
            Model.GrupoEspecial grupo = dal.GetGruposEspeciais().Where( x => x.Id == cidadao.GrupoEspecial.Id ).FirstOrDefault();
            Model.Cidadao cidadaoDal = new Model.Cidadao();
            if ( grupo != null && local != null )
            {
                cidadaoDal = dal.CreateCidadao(cidadao);
                cidadaoDal.GrupoEspecial = grupo;
                cidadaoDal.Local = local;
            }
            else
            {
                return BadRequest( "Grupo Especial ou Local não foram encontrados." );
            }
            if (cidadaoDal.Id != 0) {
                return Ok(cidadaoDal);
            }
            return BadRequest("Cidadao já cadastrado.");
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<string> EditCidadao([FromBody] Model.Cidadao cidadao, int id)
        {
            Model.DAL dal = new Model.DAL();
            Model.Cidadao cidadao2 = new Model.Cidadao
            {
                Id = id,
                DtNascimento = cidadao.DtNascimento,
                NmCidadao = cidadao.NmCidadao,
                IsNotificado = cidadao.IsNotificado,
                PrimeiraDose = cidadao.PrimeiraDose,
                SegundaDose = cidadao.SegundaDose,
                Vacina = cidadao.Vacina,
                Email = cidadao.Email,
                Local = cidadao.Local,
                GrupoEspecial = cidadao.GrupoEspecial
            };
            Model.Local local = dal.GetLocais().Where(x => x.Id == cidadao.Local.Id).FirstOrDefault();
            Model.GrupoEspecial grupo = dal.GetGruposEspeciais().Where( x => x.Id == cidadao.GrupoEspecial.Id ).FirstOrDefault();
            Model.Cidadao cidadaoDal;
            if ( grupo != null && local != null )
            {
                cidadaoDal = dal.EditCidadao(cidadao2);
                cidadaoDal.Local = local;
                cidadaoDal.GrupoEspecial = grupo;
            }
            else
            {
                return BadRequest( "Grupo Especial ou Local não foram encontrados." );
            }
            if (cidadaoDal.Id != 0)
            {
                return Ok(cidadaoDal);
            }
            return BadRequest("Cidadao não encontrado");
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult<string> DeleteVacinacao(int id)
        {
            Model.DAL dal = new Model.DAL();
           Boolean success  = dal.DeleteCidadao(id);
            if (success)
            {
                return Ok($" Cidadao de {id} foi deletado com sucesso.");
            }
            return BadRequest("Cidadao não encontrado.");
        }
    }
}

