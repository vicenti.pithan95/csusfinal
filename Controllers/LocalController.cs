﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSusAV.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocalController : ControllerBase
    {
        [HttpGet]
        public IEnumerable<Model.Local> GetLocais()
        {
            Model.DAL dal = new Model.DAL();

            List<Model.Local> listaLocais = dal.GetLocais();
            return listaLocais;
        }

        [HttpGet("{id}")]
        public Model.Local GetLocal(int id)
        {
            Model.Local c1 = new Model.Local();
            c1 = GetLocais().Where(x => x.Id == id).FirstOrDefault();
            return c1;
        }

        [HttpPost]
        [Authorize]
        public ActionResult<string> CreateUf([FromBody]Model.Local local)
        {
            Model.DAL dal = new Model.DAL();
            Model.Local c1 = dal.CreateLocal(local);
            if (!string.IsNullOrWhiteSpace(c1.NmUf)) {
                return Ok(c1);
            }
            return BadRequest("Cidade já cadastrada.");
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<string> EditUf([FromBody] Model.Local local, int id)
        {
            Model.DAL dal = new Model.DAL();
            Model.Local c1 = new Model.Local
            {
                NmUf = local.NmUf,
                SiglaUf = local.SiglaUf,
                NmCidade = local.NmCidade,
                Id = id
            };
            Model.Local localDal = dal.EditLocal(c1);
            if (!string.IsNullOrWhiteSpace(localDal.NmUf))
            {
                return Ok(c1);
            }
            return BadRequest("Cidade não encontrada");
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult<string> DeleteUf(int id)
        {
            Model.DAL dal = new Model.DAL();
           Boolean success  = dal.DeleteLocal(id);
            if (success)
            {
                return Ok($" A cidade de {id} foi deletada com sucesso.");
            }
            return BadRequest("Cidade não encontrada.");
        }
    }
}

