﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSusAV.Controllers
{
    [Route( "api/[controller]" )]
    [ApiController]
    public class VacinacaoController : ControllerBase
    {
        [HttpGet]
        [Route( "/on" )]
        public ActionResult<string> Hello()
        {
            return Ok("ta no ar." );
        }
        [HttpGet]
        public IEnumerable<Model.Vacinacao> GetVacinacoes()
        {
            Model.DAL dal = new Model.DAL();

            List<Model.Vacinacao> listaVacinacao = dal.GetVacinacoes();
            return listaVacinacao;
        }

        [HttpGet( "{id}" )]
        public Model.Vacinacao GetVacinacao( int id )
        {
            Model.DAL dal = new Model.DAL();
            Model.Vacinacao vacinacao = new Model.Vacinacao();
            vacinacao = GetVacinacoes().Where( x => x.Id == id ).FirstOrDefault();

            Model.GrupoEspecial grupo = dal.GetGruposEspeciais().Where( x => x.Id == vacinacao.GrupoEspecial.Id ).FirstOrDefault();

            Model.Local local = dal.GetLocais().Where( x => x.Id == vacinacao.Local.Id ).FirstOrDefault();

            if (grupo != null && local != null)
            {
                vacinacao.Local = local;
                vacinacao.GrupoEspecial = grupo;
            }
            else
            {
                return vacinacao;
            }
            return vacinacao;
        }

        [HttpPost]
        [Authorize]
        public ActionResult<string> CreateVacinacao( [FromBody] Model.Vacinacao vacinacao )
        {
            Model.DAL dal = new Model.DAL();
            Model.GrupoEspecial grupo = dal.GetGruposEspeciais().Where( x => x.Id == vacinacao.GrupoEspecial.Id ).FirstOrDefault();

            Model.Local local = dal.GetLocais().Where( x => x.Id == vacinacao.Local.Id ).FirstOrDefault();
            Model.Vacinacao vacinacaoDal = new Model.Vacinacao();
            if (grupo != null && local != null)
            {
                vacinacaoDal = dal.CreateVacinacao( vacinacao );
                vacinacaoDal.Local = local;
                vacinacaoDal.GrupoEspecial = grupo;
            }
            else
            {
                return BadRequest( " Grupo Especial ou Local não foram encontrados." );
            }
            return Ok( vacinacaoDal );
        }

        [HttpPut("{id}")]
        [Authorize]
        public ActionResult<string> EditVacinacao([FromBody] Model.Vacinacao vacinacao, int id)
        {
            Model.DAL dal = new Model.DAL();
            Model.Vacinacao c1 = new Model.Vacinacao
            {
                IdadeCorte = vacinacao.IdadeCorte,
                Local = vacinacao.Local,
                GrupoEspecial = vacinacao.GrupoEspecial,
                Id = id
            };
            Model.GrupoEspecial grupo =dal.GetGruposEspeciais().Where(x => x.Id == vacinacao.GrupoEspecial.Id).FirstOrDefault();

            Model.Local local = dal.GetLocais().Where(x => x.Id == vacinacao.Local.Id).FirstOrDefault();
            Model.Vacinacao vacinacaoDal;
            if (grupo !=null && local != null)
            {
                vacinacaoDal = dal.EditVacinacao(c1);
                vacinacaoDal.Local = local;
                vacinacaoDal.GrupoEspecial = grupo;
            }
            else
            {
                return BadRequest("Grupo Especial ou Local não foram encontrados.");
            }
            if (vacinacaoDal.Id != 0)
            {
                return Ok(vacinacaoDal);
            }
            return BadRequest("Vacinacao não encontrada");
        }

        [HttpDelete("{id}")]
        [Authorize]
        public ActionResult<string> DeleteVacinacao(int id)
        {
            Model.DAL dal = new Model.DAL();
           Boolean success  = dal.DeleteVacinacao(id);
            if (success)
            {
                return Ok($" Vacinacao de {id} foi deletada com sucesso.");
            }
            return BadRequest("Vacinacao não encontrada.");
        }
    }
}

