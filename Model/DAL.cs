﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
namespace ApiSusAV.Model
{
    public class DAL
    {

        private string db = "sus";
        MySqlConnection con = new MySqlConnection("server=localhost;port=3306;uid=root;pwd=123456ml-;database=sus");

        //*** Delete ***

        public Boolean DeleteCidadao(int id)
        {
            return this.Delete("cidadao", id);
        }

        public Boolean DeleteGrupoEspecial(int id)
        {
            return this.Delete("gruposvacinacao", id);
        }

        public Boolean DeleteLocal(int id)
        {
            return this.Delete("local", id);
        }

        public Boolean DeleteVacinacao(int id)
        {
            return this.Delete("vacinacao", id);
        }

        //*** Put ***

        public void EditCidadaoBot( String email, String idChatBot )
        {
            Cidadao cidadaoDto = new Cidadao();
            con.Open();
            var cmd = con.CreateCommand();
            var cmd2 = con.CreateCommand();
            cmd.CommandText = ($"SELECT * FROM `{db}`.cidadao c where c.email = '{ email }'");
            var dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                cmd2.CommandText = ($"UPDATE `{db}`.`cidadao` SET `idChatBot` = '{ idChatBot }'" +
                    $" WHERE `email` = '{ email }' ; ");
                dr.Close();
                cmd2.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                dr.Read();
                var cidadao = new Cidadao();
                cidadao.Id = dr.GetInt16( "id" );
                cidadao.NmCidadao = dr.GetString( "nmCidadao" );
                cidadao.Vacina = dr.GetString( "vacina" );
                cidadao.Email = dr.GetString( "email" );
                cidadao.IsNotificado = dr.GetBoolean( "isNotificado" );
                cidadao.DtNascimento = (DateTime)dr.GetMySqlDateTime( "dtNascimento" );
                cidadao.PrimeiraDose = (DateTime)dr.GetMySqlDateTime( "primeiraDose" );
                cidadao.SegundaDose = (DateTime)dr.GetMySqlDateTime( "segundaDose" );
                cidadaoDto = cidadao;
            }
            con.Close();
        }
       
        public Cidadao EditCidadao(Cidadao cidadaoDal)
        {
            Cidadao cidadaoDto = new Cidadao();
            con.Open();
            var cmd = con.CreateCommand();
            var cmd2 = con.CreateCommand();
            cmd.CommandText = ($"SELECT * FROM `{db}`.cidadao c where c.id = '{ cidadaoDal.Id }'");
            var dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                cmd2.CommandText = ($"UPDATE `{db}`.`cidadao` SET nmCidadao = '{ cidadaoDal.NmCidadao}', dtNascimento = '{ cidadaoDal.DtNascimento.ToString("yyyy-MM-dd") }', " +
                    $"idLocal = '{ cidadaoDal.Local.Id }',idGrupo = '{ cidadaoDal.GrupoEspecial.Id }', isNotificado = { cidadaoDal.IsNotificado }, vacina = '{ cidadaoDal.Vacina }', primeiraDose = '{ cidadaoDal.PrimeiraDose.ToString("yyyy-MM-dd") }'" +
                    $",segundaDose = '{ cidadaoDal.SegundaDose.ToString("yyyy-MM-dd") }',  email = '{ cidadaoDal.Email }'" +
                    $" WHERE `id` = { cidadaoDal.Id } ; ");
                dr.Close();
                cmd2.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                dr.Read();
                var cidadao = new Cidadao();
                cidadao.Id = dr.GetInt16("id");
                cidadao.NmCidadao = dr.GetString("nmCidadao");
                cidadao.Vacina = dr.GetString("vacina");
                cidadao.Email = dr.GetString("email");
                cidadao.IsNotificado = dr.GetBoolean("isNotificado");
                cidadao.DtNascimento = (DateTime)dr.GetMySqlDateTime("dtNascimento");
                cidadao.PrimeiraDose = (DateTime)dr.GetMySqlDateTime("primeiraDose");
                cidadao.SegundaDose = (DateTime)dr.GetMySqlDateTime("segundaDose");
                cidadaoDto = cidadao;
            }
            else
            {
                return cidadaoDto;
            }


            con.Close();
            return cidadaoDto;
        }

        public Boolean EditIsNotificadoBot( Cidadao cidadaoDal )
        {
            con.Open();
            var cmd = con.CreateCommand();
            var cmd2 = con.CreateCommand();
            cmd.CommandText = ($"SELECT * FROM `{db}`.cidadao c where c.id = '{ cidadaoDal.Id }'");
            var dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                cmd2.CommandText = ($"UPDATE `{db}`.`cidadao` SET isNotificadoBot = true" +
                    $" WHERE `id` = { cidadaoDal.Id } ; ");
                dr.Close();
                cmd2.ExecuteNonQuery();
            }
            else
            {
                return false;
            }


            con.Close();
            return true;
        }

        public Boolean EditIsNotificado( Cidadao cidadaoDal )
        {
            con.Open ();
            var cmd = con.CreateCommand ();
            var cmd2 = con.CreateCommand ();
            cmd.CommandText = ($"SELECT * FROM `{db}`.cidadao c where c.id = '{ cidadaoDal.Id }'");
            var dr = cmd.ExecuteReader ();
            if (dr.Read ())
            {
                cmd2.CommandText = ($"UPDATE `{db}`.`cidadao` SET isNotificado = true" +
                    $" WHERE `id` = { cidadaoDal.Id } ; ");
                dr.Close ();
                cmd2.ExecuteNonQuery ();
            }
            else
            {
                return false;
            }


            con.Close ();
            return true;
        }

        public GrupoEspecial EditGrupoEspecial(GrupoEspecial grupoEspecialDal)
        {
            GrupoEspecial grupoEspecialDto = new GrupoEspecial();
            con.Open();
            var cmd = con.CreateCommand();
            var cmd2 = con.CreateCommand();
            cmd.CommandText = ($"SELECT * FROM `{db}`.gruposvacinacao c where c.id = '{ grupoEspecialDal.Id }'");
            var dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                cmd2.CommandText = ($"UPDATE `{db}`.`gruposvacinacao` SET nmGrupo = '{ grupoEspecialDal.NmGrupo}', isComorbidade = { grupoEspecialDal.IsComorbidade} WHERE `id` = { grupoEspecialDal.Id } ; ");
                dr.Close();
                cmd2.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    var grupoEspecial = new GrupoEspecial();
                    grupoEspecial.Id = dr.GetInt16("id");
                    grupoEspecial.NmGrupo = dr.GetString("nmGrupo");
                    grupoEspecial.IsComorbidade = dr.GetBoolean("isComorbidade");
                    grupoEspecialDto = grupoEspecial;
                }
            }
            else
            {
                return grupoEspecialDto;
            }


            con.Close();
            return grupoEspecialDto;
        }

        public Local EditLocal(Local localDal)
        {
            Local localDto = new Local();
            con.Open();
            var cmd = con.CreateCommand();
            var cmd2 = con.CreateCommand();
            cmd.CommandText = ($"SELECT * FROM `{db}`.local c where c.id = '{ localDal.Id }'");
            var dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                cmd2.CommandText = ($"UPDATE `{db}`.`local` SET nmUf = '{ localDal.NmUf}', siglaUf = '{ localDal.SiglaUf }', nmCidade = '{ localDal.NmCidade }' WHERE `id` = { localDal.Id } ; ");
                dr.Close();
                cmd2.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    var local = new Local();
                    local.Id = dr.GetInt16("id");
                    local.NmUf = dr.GetString("nmUf");
                    local.SiglaUf = dr.GetString("siglaUf");
                    local.NmCidade = dr.GetString("nmCidade");
                    localDto = local;
                }
            }
            else
            {
                return localDto;
            }


            con.Close();
            return localDto;
        }

        public Vacinacao EditVacinacao(Vacinacao vacinacaoDal)
        {
            Vacinacao vacinacaoDto = new Vacinacao();
            con.Open();
            var cmd = con.CreateCommand();
            var cmd2 = con.CreateCommand();
            cmd.CommandText = ($"SELECT * FROM `{db}`.vacinacao c where c.id = '{ vacinacaoDal.Id }'");
            var dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                cmd2.CommandText = ($"UPDATE `{db}`.`vacinacao` SET idLocal = { vacinacaoDal.Local.Id }, idGrupo = { vacinacaoDal.GrupoEspecial.Id }," +
                                    $" idadeCorte = { vacinacaoDal.IdadeCorte }" +
                                    $" WHERE `id` = { vacinacaoDal.Id } ; ");
                dr.Close();
                cmd2.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                dr.Read();
                var vacinacao = new Vacinacao();
                vacinacao.Id = dr.GetInt16("id");
                vacinacao.IdadeCorte = dr.GetInt16( "idadeCorte" );
                vacinacaoDto = vacinacao;
            }
            else
            {
                return vacinacaoDto;
            }


            con.Close();
            return vacinacaoDto;
        }

        //*** Post ***

        public User CreateUser(User userDal)
        {
            User userDto = new User();
            con.Open();
            var cmd = con.CreateCommand();
            var cmd2 = con.CreateCommand();
            cmd.CommandText = ($"SELECT * FROM `{db}`.user u where u.username = '{ userDal.Username }';");
            var dr = cmd.ExecuteReader();
            if (!dr.Read())
            {
                cmd2.CommandText = ($"INSERT INTO `{db}`.user (`username`,`password`,`role`)" +
                    $" VALUES ('{ userDal.Username }', '{ userDal.Password }', '{ userDal.Role }');");
                dr.Close();
                cmd2.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                dr.Read();
                var user = new User();
                user.Id = dr.GetInt32("id");
                user.Username = dr.GetString("username");
                user.Password = "";
                user.Role = dr.GetString("role");
                userDto = user;
            }
            else
            {
                return userDto;
            }


            con.Close();
            return userDto;
        }

        public Cidadao CreateCidadao(Cidadao cidadaoDal)
        {
            Cidadao cidadaoDto = new Cidadao();
            con.Open();
            var cmd = con.CreateCommand();
            var cmd2 = con.CreateCommand();
            cmd.CommandText = ($"SELECT * FROM `{db}`.cidadao c where c.email = '{ cidadaoDal.Email }';") ;
            var dr = cmd.ExecuteReader();
            if (!dr.Read())
            {
                cmd2.CommandText = ($"INSERT INTO `{db}`.cidadao (`nmcidadao`,`dtNascimento`,`idLocal`,`isNotificado`,`Vacina`,`primeiraDose`,`SegundaDose`,`email`,`idGrupo` )" +
                    $" VALUES ('{ cidadaoDal .NmCidadao }', '{ cidadaoDal.DtNascimento.ToString("yyyy-MM-dd") }', { cidadaoDal.Local.Id },{ cidadaoDal.IsNotificado }," +
                    $"'{ cidadaoDal.Vacina }', '{ cidadaoDal.PrimeiraDose.ToString("yyyy-MM-dd") }', '{ cidadaoDal.SegundaDose.ToString("yyyy-MM-dd") }','{ cidadaoDal.Email }',{ cidadaoDal.GrupoEspecial.Id });");
                dr.Close();
                cmd2.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                dr.Read();
                var cidadao = new Cidadao();
                cidadao.Id = dr.GetInt16("id");
                cidadao.NmCidadao = dr.GetString("nmCidadao");
                cidadao.Vacina = dr.GetString("vacina");
                cidadao.Email = dr.GetString("email");
                cidadao.IsNotificado = dr.GetBoolean("isNotificado");
                cidadao.DtNascimento = (DateTime)dr.GetMySqlDateTime("dtNascimento");
                cidadao.PrimeiraDose = (DateTime)dr.GetMySqlDateTime("primeiraDose");
                cidadao.SegundaDose = (DateTime)dr.GetMySqlDateTime("segundaDose");
                cidadaoDto = cidadao;
            }
            else
            {
                return cidadaoDto;
            }


            con.Close();
            return cidadaoDto;
        }

        public GrupoEspecial CreateGrupoEspecial(GrupoEspecial grupoEspecialDal)
        {
            GrupoEspecial grupoEspecialDto = new GrupoEspecial();
            con.Open();
            var cmd = con.CreateCommand();
            var cmd2 = con.CreateCommand();
            cmd.CommandText = ($"SELECT * FROM `{db}`.gruposvacinacao c where c.nmGrupo = '{ grupoEspecialDal.NmGrupo }'");
            var dr = cmd.ExecuteReader();
            if (!dr.Read())
            {
                cmd2.CommandText = ($"INSERT INTO `{db}`.gruposvacinacao (nmGrupo, isComorbidade) VALUES ('{ grupoEspecialDal.NmGrupo }', { grupoEspecialDal.IsComorbidade });");
                dr.Close();
                cmd2.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    var grupoEspecial = new GrupoEspecial();
                    grupoEspecial.Id = dr.GetInt16("id");
                    grupoEspecial.NmGrupo = dr.GetString("nmGrupo");
                    grupoEspecial.IsComorbidade = dr.GetBoolean("isComorbidade");
                    grupoEspecialDto = grupoEspecial;
                }
            }
            else
            {
                return grupoEspecialDto;
            }


            con.Close();
            return grupoEspecialDto;
        }

        public Local CreateLocal(Local localDal)
        {
            Local localDto = new Local();
            con.Open();
            var cmd = con.CreateCommand();
            var cmd2 = con.CreateCommand();
            cmd.CommandText = ($"SELECT * FROM `{db}`.local c where c.nmUf = '{ localDal.NmUf }' and c.nmCidade = '{ localDal.NmCidade }' and c.siglaUf = '{ localDal.SiglaUf }'");
            var dr = cmd.ExecuteReader();
            if (!dr.Read())
            {
                cmd2.CommandText = ($"INSERT INTO `{db}`.local (`nmUf`,`siglaUf`, `nmCidade`) VALUES ('{ localDal.NmUf }', '{ localDal.SiglaUf }', '{ localDal.NmCidade }');");
                dr.Close();
                cmd2.ExecuteNonQuery();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    var local = new Local();
                    local.Id = dr.GetInt16("id");
                    local.NmUf = dr.GetString("nmUf");
                    local.SiglaUf = dr.GetString("siglaUf");
                    local.NmCidade = dr.GetString("nmCidade");
                    localDto = local;
                }
            }
            else
            {
                return localDto;
            }


            con.Close();
            return localDto;
        }

        public Vacinacao CreateVacinacao(Vacinacao vacinacaoDal)
        {
           Vacinacao vacinacaoDto = new Vacinacao();
            con.Open();
            var cmd = con.CreateCommand();
            var cmd2 = con.CreateCommand();
            cmd.CommandText = ($"SELECT * FROM `{db}`.vacinacao c where (c.idLocal = { vacinacaoDal.Local.Id } and idGrupo = { vacinacaoDal.GrupoEspecial.Id })");
            var dr = cmd.ExecuteReader();
            if (!dr.Read()) {
                cmd2.CommandText = ($"INSERT INTO `{db}`.vacinacao (`idLocal`,`idGrupo`, `idadeCorte`)" +
                    $" VALUES ({ vacinacaoDal.Local.Id }, { vacinacaoDal.GrupoEspecial.Id }," +
                    $" { vacinacaoDal.IdadeCorte });");
            }
            else
            {
                cmd2.CommandText = ($"UPDATE `{db}`.`vacinacao` SET idLocal = { vacinacaoDal.Local.Id }, idGrupo = { vacinacaoDal.GrupoEspecial.Id }," +
                                $" idadeCorte = { vacinacaoDal.IdadeCorte }" +
                                $" WHERE ( `{db}`.`vacinacao`.`idLocal` = { vacinacaoDal.Local.Id } and  `{db}`.`vacinacao`.`idGrupo` = { vacinacaoDal.GrupoEspecial.Id }) ; ");
            }
            dr.Close();
            cmd2.ExecuteNonQuery();
            dr = cmd.ExecuteReader();
            dr.Read();
            var vacinacao = new Vacinacao();
            vacinacao.Id = dr.GetInt16("id");
            vacinacao.IdadeCorte = dr.GetInt16( "idadeCorte" );
            vacinacaoDto = vacinacao;
            con.Close();
            return vacinacaoDto;
        }

        //*** Get ***

        public User GetUser(String userDal)
        {
            User userDto = new User();
            con.Open();
            var cmd = con.CreateCommand();
            var cmd2 = con.CreateCommand();
            cmd.CommandText = ($"SELECT * FROM `{db}`.user u where u.username = '{ userDal }';");
            var dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                var user = new User();
                user.Id = dr.GetInt32("id");
                user.Username = dr.GetString("username");
                user.Password = "";
                user.Role = dr.GetString("role");
                userDto = user;
            }
            else
            {
                return userDto;
            }


            con.Close();
            return userDto;
        }

        public List<Cidadao> GetCidadaos()
        {
            List<Cidadao> listCidadaoDto = new List<Cidadao>();
            con.Open();
            var cmd = con.CreateCommand();
            cmd.CommandText = ($"SELECT * FROM `{db}`.cidadao");
            var dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                var cidadao = new Cidadao();
                var local = new Local();
                var grupoEspecial = new GrupoEspecial();
                cidadao.Id = dr.GetInt16("id");
                cidadao.NmCidadao = dr.GetString("nmCidadao");
                cidadao.Vacina = dr.GetString("vacina");
                cidadao.Email = dr.GetString("email");
                cidadao.IsNotificado = dr.GetBoolean("isNotificado");
                cidadao.DtNascimento = (DateTime)dr.GetMySqlDateTime("dtNascimento");
                cidadao.PrimeiraDose = (DateTime)dr.GetMySqlDateTime("primeiraDose");
                cidadao.SegundaDose = (DateTime)dr.GetMySqlDateTime("segundaDose");
                local.Id = dr.GetInt16("idLocal");
                cidadao.isNotificadoBot = dr.GetBoolean( "isNotificadoBot" );
                cidadao.Local = local;
                grupoEspecial.Id = dr.GetInt16( "idGrupo" );
                cidadao.GrupoEspecial = grupoEspecial;
                listCidadaoDto.Add(cidadao);
            }
            dr.Close();
            con.Close();
            return listCidadaoDto;
        }

        public List<Cidadao> GetCidadaosBot()
        {
            List<Cidadao> listCidadaoDto = new List<Cidadao>();
            con.Open();
            var cmd = con.CreateCommand();
            cmd.CommandText = ($"SELECT * FROM `{db}`.cidadao where cidadao.idChatBot is not null;");
            var dr = cmd.ExecuteReader();
            Local local = new Local();
            GrupoEspecial grupoEspecial = new GrupoEspecial();
            while (dr.Read())
            {
                var cidadao = new Cidadao();
                cidadao.Id = dr.GetInt16( "id" );
                cidadao.NmCidadao = dr.GetString( "nmCidadao" );
                cidadao.Vacina = dr.GetString( "vacina" );
                cidadao.Email = dr.GetString( "email" );
                cidadao.IsNotificado = dr.GetBoolean( "isNotificado" );
                cidadao.DtNascimento = (DateTime)dr.GetMySqlDateTime( "dtNascimento" );
                cidadao.PrimeiraDose = (DateTime)dr.GetMySqlDateTime( "primeiraDose" );
                cidadao.SegundaDose = (DateTime)dr.GetMySqlDateTime( "segundaDose" );
                local.Id = dr.GetInt16( "idLocal" );
                cidadao.IdChatBot = dr.GetString( "idChatBot" );
                cidadao.IsNotificado = dr.GetBoolean( "isNotificado" );
                cidadao.Local = local;
                grupoEspecial.Id = dr.GetInt16( "idGrupo" );
                cidadao.GrupoEspecial = grupoEspecial;
                listCidadaoDto.Add( cidadao );
            }
            dr.Close();
            con.Close();
            return listCidadaoDto;
        }

        public Boolean EmailDuplicado( String email)
        {
            con.Open();
            var cmd = con.CreateCommand();
            cmd.CommandText = ($"SELECT * FROM cidadao where `{db}`.cidadao.email = '{ email }';");
            var dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                return true;
            }
            dr.Close();
            con.Close();
            return false;
        }
        
        public List<Cidadao> Notificacoesbot()
        {
            List<Cidadao> listCidadaoDto = new List<Cidadao>();
            con.Open();
            var cmd = con.CreateCommand();
            cmd.CommandText = ($"SELECT distinct `cidadao`.`id`,`cidadao`.`nmcidadao`,`cidadao`.`dtNascimento`,`cidadao`.`idLocal`," +
                $"`cidadao`.`isNotificado`,`cidadao`.`Vacina`,`cidadao`.`primeiraDose`,`cidadao`.`SegundaDose`,`cidadao`.`email`,`cidadao`.`idGrupo`," +
                $" `cidadao`.`isNotificadoBot`, `cidadao`.`idChatBot` FROM `{db}`.cidadao" +
                $" inner join `{db}`.vacinacao on `{db}`.cidadao.idLocal = `{db}`.vacinacao.idLocal" +
                $" inner join `{db}`.gruposvacinacao on `{db}`.cidadao.idGrupo = `{db}`.gruposvacinacao.id" +
                $" WHERE ((TIMESTAMPDIFF (YEAR,dtNascimento ,CURDATE()) >= vacinacao.idadeCorte) and " +
                $"( `{db}`.cidadao.idGrupo = `{db}`.vacinacao.idGrupo and `{db}`.cidadao.idLocal = `{db}`.vacinacao.idLocal ) " +
                $"and (`{db}`.cidadao.isNotificadoBot = false)" +
                $" and (`{db}`.`cidadao`.`idChatBot` is not null))" +
                $"or" +
                $"((TIMESTAMPDIFF (YEAR,dtNascimento ,CURDATE()) >= vacinacao.idadeCorte) and " +
                $"( `{db}`.gruposvacinacao.nmGrupo ='GERAL' and `{db}`.cidadao.idLocal = `{db}`.vacinacao.idLocal) and " +
                $"(`{db}`.cidadao.isNotificadoBot = false) " +
                $"and (`{db}`.`cidadao`.`idChatBot` is not null));");

            var dr = cmd.ExecuteReader();
            Local local = new Local();
            while (dr.Read())
            {
                var cidadao = new Cidadao();
                cidadao.Id = dr.GetInt16( "id" );
                cidadao.NmCidadao = dr.GetString( "nmCidadao" );
                cidadao.Vacina = dr.GetString( "vacina" );
                cidadao.Email = dr.GetString( "email" );
                cidadao.IdChatBot = dr.GetString( "idChatBot" );
                cidadao.IsNotificado = dr.GetBoolean( "isNotificado" );
                cidadao.isNotificadoBot = dr.GetBoolean( "isNotificadoBot" );
                cidadao.DtNascimento = (DateTime)dr.GetMySqlDateTime( "dtNascimento" );
                cidadao.PrimeiraDose = (DateTime)dr.GetMySqlDateTime( "primeiraDose" );
                cidadao.SegundaDose = (DateTime)dr.GetMySqlDateTime( "segundaDose" );
                local.Id = dr.GetInt16( "idLocal" );
                cidadao.Local = local;
                listCidadaoDto.Add( cidadao );
            }
            dr.Close();
            con.Close();
            return listCidadaoDto;
        }

        public List<Cidadao> Ntificacoes()
        {
            List<Cidadao> listCidadaoDto = new List<Cidadao> ();
            con.Open ();
            var cmd = con.CreateCommand ();
            cmd.CommandText = ($"SELECT distinct `cidadao`.`id`,`cidadao`.`nmcidadao`,`cidadao`.`dtNascimento`,`cidadao`.`idLocal`," +
                $"`cidadao`.`isNotificado`,`cidadao`.`Vacina`,`cidadao`.`primeiraDose`,`cidadao`.`SegundaDose`,`cidadao`.`email`,`cidadao`.`idGrupo`" +
                $"FROM `{db}`.cidadao" +
                $" inner join `{db}`.vacinacao on `{db}`.cidadao.idLocal = `{db}`.vacinacao.idLocal" +
                $" inner join `{db}`.gruposvacinacao on `{db}`.cidadao.idGrupo = `{db}`.gruposvacinacao.id" +
                $" WHERE ((TIMESTAMPDIFF (YEAR,dtNascimento ,CURDATE()) >= vacinacao.idadeCorte) and " +
                $"( `{db}`.cidadao.idGrupo = `{db}`.vacinacao.idGrupo and `{db}`.cidadao.idLocal = `{db}`.vacinacao.idLocal ) " +
                $"and `{db}`.cidadao.isNotificado = false)" +
                $"or" +
                $"((TIMESTAMPDIFF (YEAR,dtNascimento ,CURDATE()) >= vacinacao.idadeCorte) and " +
                $"( `{db}`.gruposvacinacao.nmGrupo ='GERAL' and `{db}`.cidadao.idLocal = `{db}`.vacinacao.idLocal) and " +
                $"`{db}`.cidadao.isNotificado = false);");

            var dr = cmd.ExecuteReader ();
            Local local = new Local ();
            while (dr.Read ())
            {
                var cidadao = new Cidadao ();
                cidadao.Id = dr.GetInt16 ( "id" );
                cidadao.NmCidadao = dr.GetString ( "nmCidadao" );
                cidadao.Vacina = dr.GetString ( "vacina" );
                cidadao.Email = dr.GetString ( "email" );
                cidadao.IsNotificado = dr.GetBoolean ( "isNotificado" );
                cidadao.DtNascimento = (DateTime)dr.GetMySqlDateTime ( "dtNascimento" );
                cidadao.PrimeiraDose = (DateTime)dr.GetMySqlDateTime ( "primeiraDose" );
                cidadao.SegundaDose = (DateTime)dr.GetMySqlDateTime ( "segundaDose" );
                local.Id = dr.GetInt16 ( "idLocal" );
                cidadao.Local = local;
                listCidadaoDto.Add ( cidadao );
            }
            dr.Close ();
            con.Close ();
            return listCidadaoDto;
        }

        public Boolean Vacinacao( String email )
        {
            con.Open();
            var cmd = con.CreateCommand();
            cmd.CommandText = ($"SELECT distinct `cidadao`.`id`,`cidadao`.`nmcidadao`,`cidadao`.`dtNascimento`,`cidadao`.`idLocal`," +
                $"`cidadao`.`isNotificado`,`cidadao`.`Vacina`,`cidadao`.`primeiraDose`,`cidadao`.`SegundaDose`,`cidadao`.`email`,`cidadao`.`idGrupo`" +
                $"FROM `{db}`.cidadao" +
                $" inner join `{db}`.vacinacao on `{db}`.cidadao.idLocal = `{db}`.vacinacao.idLocal" +
                $" inner join `{db}`.gruposvacinacao on `{db}`.cidadao.idGrupo = `{db}`.gruposvacinacao.id" +
                $" WHERE (((TIMESTAMPDIFF (YEAR,dtNascimento ,CURDATE()) >= vacinacao.idadeCorte) and " +
                $"( `{db}`.cidadao.idGrupo = `{db}`.vacinacao.idGrupo and `{db}`.cidadao.idLocal = `{db}`.vacinacao.idLocal ))" +
                $"or" +
                $" ((TIMESTAMPDIFF (YEAR,dtNascimento ,CURDATE()) >= vacinacao.idadeCorte) and " +
                $"( `{db}`.gruposvacinacao.nmGrupo = 'GERAL' and `{db}`.cidadao.idLocal =`{db}`.vacinacao.idLocal))" +
                $"or" +
                $" `{db}`.gruposvacinacao.isComorbidade = true)" +
                $" and `{db}`.cidadao.email = '{ email }';");

            var dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<GrupoEspecial> GetGruposEspeciais()
        {
            List<GrupoEspecial> listGruposEspeciais = new List<GrupoEspecial>();
            con.Open();
            var cmd = con.CreateCommand();
            cmd.CommandText = "SELECT * FROM gruposvacinacao";
            var dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                var grupoEspecial = new GrupoEspecial();
                grupoEspecial.Id = dr.GetInt16("id");
                grupoEspecial.NmGrupo = dr.GetString("nmGrupo");
                grupoEspecial.IsComorbidade = dr.GetBoolean("isComorbidade");
                listGruposEspeciais.Add(grupoEspecial);
            }
            dr.Close();
            con.Close();
            return listGruposEspeciais;
        }

        public List<GrupoEspecial> GetGruposEspecial(String grupo)
        {
            List<GrupoEspecial> listGruposEspeciais = new List<GrupoEspecial>();
            con.Open();
            var cmd = con.CreateCommand();
            cmd.CommandText = ($"SELECT * FROM `{db}`.user u where u.username = '{ grupo }';");
            var dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                var grupoEspecial = new GrupoEspecial();
                grupoEspecial.Id = dr.GetInt16("id");
                grupoEspecial.NmGrupo = dr.GetString("nmGrupo");
                grupoEspecial.IsComorbidade = dr.GetBoolean("isComorbidade");
                listGruposEspeciais.Add(grupoEspecial);
            }
            dr.Close();
            con.Close();
            return listGruposEspeciais;
        }

        public List<Local> GetLocais()
        {
            List<Local> listLocaisDto = new List<Local>();
            con.Open();
            var cmd = con.CreateCommand();
            cmd.CommandText = ($"SELECT * FROM `{db}`.local");
            var dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                var local = new Local();
                local.Id = dr.GetInt16("id");
                local.NmUf = dr.GetString("nmUf");
                local.SiglaUf = dr.GetString("siglaUf");
                local.NmCidade = dr.GetString("nmCidade");
                listLocaisDto.Add(local);
            }
            dr.Close();
            con.Close();
            return listLocaisDto;
        }

        public List<Vacinacao> GetVacinacoes()
        {
            List<Vacinacao> listVacinacaoDto = new List<Vacinacao>();
            con.Open();
            var cmd = con.CreateCommand();
            cmd.CommandText = ($"SELECT * FROM `{db}`.vacinacao");
            var dr = cmd.ExecuteReader();
            Local local = new Local();
            GrupoEspecial grupoEspecial = new GrupoEspecial();
            while (dr.Read())
            {
                var vacinacao = new Vacinacao();
                vacinacao.Id = dr.GetInt16("id");
                local.Id = dr.GetInt16("idLocal");
                vacinacao.Local = local;
                grupoEspecial.Id = dr.GetInt16( "idGrupo" );
                vacinacao.GrupoEspecial = grupoEspecial;
                vacinacao.IdadeCorte = dr.GetInt16( "idadeCorte" );
                listVacinacaoDto.Add(vacinacao);
            }
            dr.Close();
            con.Close();
            return listVacinacaoDto;
        }

        public List<User> GetUsers()
        {
            {
                List<User> listuserDto = new List<User>();
                con.Open();
                var cmd = con.CreateCommand();
                cmd.CommandText = ($"SELECT * FROM `{db}`.user");
                var dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    var user = new User();
                    user.Id = dr.GetInt16( "id" );
                    user.Username = dr.GetString( "username" );
                    user.Password = dr.GetString( "password" );
                    user.Role = dr.GetString( "role" );
                    listuserDto.Add( user );
                }
                dr.Close();
                con.Close();
                return listuserDto;
            }
        }

        Boolean Delete(String dataBase, int id)
        {
            con.Open();
            Boolean success;
            var cmd = con.CreateCommand();
            var cmd2 = con.CreateCommand();
            cmd.CommandText = ($"SELECT * FROM `{db}`.{dataBase} c where c.id = { id }");
            var dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                cmd2.CommandText = ($"DELETE FROM `{db}`.`{dataBase}` WHERE `id` = { id } ; ");
                dr.Close();
                cmd2.ExecuteNonQuery();
                success = true;
            }
            else
            {
                success = false;
            }
            con.Close();
            return success;
        }
    }
}
