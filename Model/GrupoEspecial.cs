﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSusAV.Model
{
    public class GrupoEspecial
    {
        public int Id { get; set; }
        public string NmGrupo { get; set; }
        public Boolean IsComorbidade { get; set; }
    }
}
