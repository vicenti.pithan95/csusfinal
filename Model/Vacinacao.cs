﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSusAV.Model
{
    public class Vacinacao
    {
        public int Id { get; set; }
        public Local Local { get; set; }
        public GrupoEspecial GrupoEspecial { get; set; }
        public int IdadeCorte { get; set; }
    }
}
