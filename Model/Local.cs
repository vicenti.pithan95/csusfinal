﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSusAV.Model
{
    public class Local
    {
        public int Id { get; set; }
        public string NmUf { get; set; }
        public string SiglaUf { get; set; }
        public string NmCidade { get; set; }
    }
}
