﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSusAV.Model
{
    public class Cidadao
    {
        public int Id { get; set; }
        public string NmCidadao { get; set; }
        public DateTime DtNascimento { get; set; }
        public String Email { get; set; }
        public DateTime PrimeiraDose{ get; set; }
        public DateTime SegundaDose { get; set; }
        public String Vacina2 { get; set; }
        public Local Local { get; set; }
        public GrupoEspecial GrupoEspecial { get; set; }
        public String Vacina { get; set; }
        public Boolean IsNotificado { get; set; }
        public String IdChatBot { get; set; }
        public Boolean isNotificadoBot { get; set; }
    }
}
