﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Telegram.Bot.Args;
using Telegram.Bot;

namespace ApiSusAV.Service
{
    public class Bot
    {
        static ITelegramBotClient botClient;

        [Obsolete]
        public void StartBot()
        {
            botClient = new TelegramBotClient( "1862703522:AAFjPAUo5WuHuKcz5hE0UUrdN5F3dF8AqBc" );
            var me = botClient.GetMeAsync().Result;
            botClient.OnMessage += Bot_OnMessage;
            botClient.StartReceiving();
        }
        public async void Bot_SendMessage()
        {
            Model.DAL dal = new Model.DAL();
            List<Model.Cidadao> listaCidadao = dal.Notificacoesbot();
            Controllers.CidadaoController c = new Controllers.CidadaoController();
            foreach (Model.Cidadao cidadao in listaCidadao)
            {
                Model.Cidadao cidadaoBot = cidadao;
                cidadaoBot = c.GetCidadaoBot( cidadao.Id );
                Model.Local local = new Model.Local();
                local = dal.GetLocais().Where( x => x.Id == cidadao.Local.Id ).FirstOrDefault();

                if (local != null)
                {
                    cidadao.Local = local;
                }
                object send = await botClient.SendTextMessageAsync(
                chatId: cidadaoBot.IdChatBot,
                text: ($"Olá senhor(a) { cidadao.NmCidadao }, você já pode se vacinar na cidade de { cidadao.Local.NmCidade } " +
                $"no estado do {cidadao.Local.NmUf}.\n"));
                dal.EditIsNotificadoBot( cidadaoBot );
            }
        }
        [Obsolete]
        static async void Bot_OnMessage( object sender, MessageEventArgs e )
        {
            string isEmail = e.Message.Text;

            Regex rg = new Regex( @"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$" );

            Controllers.CidadaoController cidadadao = new Controllers.CidadaoController();
            if (e.Message.Text != null && !!rg.IsMatch( isEmail ))
            {
                Model.DAL dal = new Model.DAL();
                String email = e.Message.Text;
                if (cidadadao.VacinacaoBot( e.Message.Text ) && cidadadao.CidadaoCadastrado( e.Message.Text ))
                {
                    dal.EditCidadaoBot( email, e.Message.Chat.Id.ToString() );
                    Console.WriteLine( $"Received a text message in chat {e.Message.Chat.Id}." );
                    await botClient.SendTextMessageAsync(
                      chatId: e.Message.Chat,
                      text: "Você já pode se vacinar!\n"
                    );
                }
                else if (!cidadadao.VacinacaoBot( e.Message.Text ) && cidadadao.CidadaoCadastrado( e.Message.Text ))
                {
                    dal.EditCidadaoBot( email, e.Message.Chat.Id.ToString() );
                    await botClient.SendTextMessageAsync(
                     chatId: e.Message.Chat,
                     text: "Você ainda não pode se vacinar!\n"
                     );
                }
                else
                {
                    await botClient.SendTextMessageAsync(
                    chatId: e.Message.Chat,
                    text: "Você não está cadastrado! Por favor acesse:\n https://testesusrs.herokuapp.com" );
                }
            }
            else
            {
                await botClient.SendTextMessageAsync(
                chatId: e.Message.Chat,
                text: "Bem vindo ao TeVacinaTche! Por favor, informe o seu endereço de e-mail.\n" );
            }
        }
    }
}
