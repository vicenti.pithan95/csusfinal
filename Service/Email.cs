﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
namespace ApiSusAV.Service
{
    public class Email
    {
       public void Notification()
        {
            Model.DAL dal = new Model.DAL ();
            List<Model.Cidadao> listaCidadao = dal.Ntificacoes();
            Controllers.CidadaoController c = new Controllers.CidadaoController ();
            foreach (Model.Cidadao cidadao in listaCidadao ){
                Model.Cidadao cidadaoEmail = cidadao;
                cidadaoEmail = c.GetCidadao ( cidadao.Id);
                if(SendEmail(cidadaoEmail).Equals( "success" ))
                {
                    dal.EditIsNotificado ( cidadaoEmail );
                }
                
            }
        }
        public string SendEmail( Model.Cidadao cidadao )
        {
            
            var fromAddress = new MailAddress("susuniscsus@gmail.com", "TeVacinaTche    ");
            var toAddress = new MailAddress ( cidadao.Email.ToString (), cidadao.NmCidadao);
            const string fromPassword = "123456ml-";
            const string subject = "TeVacinaTche";
            string body = this.BuildBody(cidadao) ;

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };
            using (var message = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
            {
                try
                {
                    smtp.Send ( message );
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
                return "success";
            }
        }
        public String BuildBody( Model.Cidadao cidadao )
        {
            string local;
            if(cidadao.Local.NmCidade != null)
            {
                local = ($" Você já pode se vacinar na cidade de { cidadao.Local.NmCidade } no Estado do { cidadao.Local.NmUf }.");
            }
            else
            {
                local = ($" Você já pode se vacinar no estado do { cidadao.Local.NmUf }.");
            }
            String body = ($"<html><body>Olá, Senhor(a) <b>{ cidadao.NmCidadao }</b>. <br>" +
                $"{ local }" +
                $"<br>Obrigado!</body></html>");

            return body;
        }
    }
}
