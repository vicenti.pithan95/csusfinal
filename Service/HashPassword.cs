﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiSusAV.Service
{
    public class HashPassword
    {
        public static string CalculaHash(string Senha)
        {
            try
            {
                System.Security.Cryptography.SHA512 sha512 = System.Security.Cryptography.SHA512.Create();
                byte[] dadosAsBytes = Convert.FromBase64String(Senha);
                String senhaDecode = System.Text.Encoding.ASCII.GetString(dadosAsBytes);
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(senhaDecode);
                byte[] hash = sha512.ComputeHash(inputBytes);
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                for (int i = 0; i < hash.Length; i++)
                {
                    sb.Append(hash[i].ToString("X2"));
                }
                return sb.ToString(); // Retorna senha criptografada 
            }
            catch (Exception)
            {
                return null; // Caso encontre erro retorna nulo
            }
        }
    }
}
