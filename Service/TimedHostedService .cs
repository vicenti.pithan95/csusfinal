﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Hosting;
using ApiSusAV.Service;

public class TimedHostedService : IHostedService, IDisposable
{
    private int executionCount = 0;
    private readonly ILogger<TimedHostedService> _logger;
    private Timer _timer;

    public TimedHostedService( ILogger<TimedHostedService> logger )
    {
        _logger = logger;
    }

    public Task StartAsync( CancellationToken stoppingToken )
    {
        _logger.LogInformation( "Timed Hosted Service running." );

        _timer = new Timer( DoWork, null, TimeSpan.Zero,
            TimeSpan.FromSeconds ( 20 ) );

        return Task.CompletedTask;
    }

    private void DoWork( object state )
    {
        var count = Interlocked.Increment( ref executionCount );
        Email email = new Email();
        Bot bot = new Bot();
        bot.Bot_SendMessage();
        email.Notification();
        _logger.LogInformation (
            "Timer de notificações Count: {Count}", count );
    }

    public Task StopAsync( CancellationToken stoppingToken )
    {
        _logger.LogInformation ( "Timed Hosted Service is stopping." );

        _timer?.Change ( Timeout.Infinite, 0 );

        return Task.CompletedTask;
    }

    public void Dispose()
    {
        _timer?.Dispose ();
    }
}